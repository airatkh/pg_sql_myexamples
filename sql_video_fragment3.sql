SELECT DISTINCT car_uid, to_char(updated_at, 'dd.mm.yyyy') update_data
FROM "wj_video_fragment" "wj_video_fragment"
WHERE ((wj_video_fragment."deleted_at" is NULL)
         AND ("wj_video_fragment"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'))
         AND ("wj_video_fragment"."updated_at" >= to_timestamp('1392529399')
         AND "wj_video_fragment"."updated_at" <= to_timestamp('1392788599'))
GROUP BY car_uid, updated_at
ORDER BY "update_data" DESC


SELECT DISTINCT car_uid, to_char(updated_at, 'dd.mm.yyyy')
FROM "wj_video_fragment" "wj_video_fragment"
WHERE ((wj_video_fragment."deleted_at" is NULL) 
        AND ("wj_video_fragment"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'))
        AND ("wj_video_fragment"."updated_at" >= to_timestamp('1292527428')
        and "wj_video_fragment"."updated_at" <= to_timestamp('1392786628'))
GROUP BY car_uid, updated_at

/*ORDER BY "updated_at" DESC*/


SELECT "wj_video_fragment"."car_uid" AS "t0_c6",
       "wj_video_fragment"."uuid" AS "t0_c0",
       "car"."vendor" AS "t1_c3",
       "car"."uuid" AS "t1_c0"
FROM "wj_video_fragment" "wj_video_fragment"
LEFT OUTER JOIN "wj_car" "car" ON ("wj_video_fragment"."car_uid"="car"."uuid") AND ("car"."deleted_at" is NULL)

WHERE (((wj_video_fragment."deleted_at" is NULL) 
        AND ("wj_video_fragment"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'))
        AND ("wj_video_fragment"."updated_at" >= to_timestamp('1392525437') and "wj_video_fragment"."updated_at" <= to_timestamp('1392784637')))
GROUP BY car_uid, vendor
ORDER BY "wj_video_fragment"."updated_at" DESC

select car_uid, to_char(start_dt, 'dd.mm.yyyy') dt, to_char(start_dt, 'yyyy-mm-dd') dt_for_sort
from wj_video_fragment 
where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') 
and user_uid ='1b2af82d-2d9e-5d03-9da4-bec03904e704'
group by to_char(start_dt, 'dd.mm.yyyy'), car_uid, to_char(start_dt, 'yyyy-mm-dd') 


select t.car_uid,  t.dt, c.vendor, c.model
from (
select car_uid, to_char(start_dt, 'dd.mm.yyyy') dt, to_char(start_dt, 'yyyy-mm-dd') dt_for_sort
from wj_video_fragment 
where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') 
and user_uid ='1b2af82d-2d9e-5d03-9da4-bec03904e704'
group by to_char(start_dt, 'dd.mm.yyyy'), car_uid, to_char(start_dt, 'yyyy-mm-dd') 
) t, wj_car c
where c.uuid = t.car_uid
order by t.dt_for_sort


select car_uid, to_char(start_dt, 'dd.mm.yyyy')
from wj_video_fragment
where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') 
group by to_char(start_dt, 'dd.mm.yyyy'), car_uid
order by to_char(start_dt, 'dd.mm.yyyy');


/*SELECT * FROM wj_video_fragment ORDER BY updated_at DESC;*/

SELECT video_fragment.uuid
FROM wj_video_fragment as video_fragment
INNER JOIN wj_track as track ON video_fragment.track_uid = track.uuid
INNER JOIN wj_device as device ON track.device_uid = device.uuid
WHERE video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704';

