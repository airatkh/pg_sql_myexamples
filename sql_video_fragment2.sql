SELECT
  "wj_track"."user_uid" as userID,
  "wj_track"."uuid" as trackID,
  (extract(epoch FROM "wj_track"."finish_dt") - extract(epoch FROM "wj_track"."start_dt")) as track_duration,
  "wj_point"."speed" as speed,
  extract(epoch FROM "wj_point"."date_time") as seconds_datetime
FROM "wj_track"
 JOIN "wj_point"
ON "wj_point"."track_uid" = "wj_track"."uuid"
WHERE
  "wj_track"."user_uid" = 'fc940970-4e38-5334-baf0-acc656f553d6' AND
  "wj_track"."start_dt" IS NOT NULL AND
  "wj_track"."finish_dt" IS NOT NULL
ORDER BY "wj_point"."date_time" ASC




SELECT
   "wj_track"."user_uid" as userID,
   "wj_track"."uuid" as trackID,
   (extract(epoch FROM "wj_track"."finish_dt") - extract(epoch FROM "wj_track"."start_dt")) as track_duration,
   "wj_ecu"."value" as speed,
   extract(epoch FROM "wj_ecu"."date_time") as seconds_speedtime
FROM "wj_track"
JOIN "wj_ecu"
ON "wj_ecu"."track_uid" = "wj_track"."uuid"
WHERE
 "wj_track"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
 AND "wj_track"."start_dt" IS NOT NULL
 AND "wj_track"."finish_dt" IS NOT NULL
 AND "wj_ecu"."code" = 'VHLSPD'
 AND "wj_ecu"."value" > '0'
ORDER BY "wj_ecu"."date_time" ASC




SELECT
   "wj_track"."user_uid" as userID,
   "wj_track"."uuid" as trackID,
   (extract(epoch FROM "wj_track"."finish_dt") - extract(epoch FROM "wj_track"."start_dt")) as track_duration,
   "wj_ecu"."value" as speed,
   extract(epoch FROM "wj_ecu"."date_time") as seconds_speedtime
FROM "wj_track"
JOIN "wj_ecu"
ON "wj_ecu"."track_uid" = "wj_track"."uuid"
WHERE
 "wj_track"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
 AND "wj_track"."start_dt" IS NOT NULL
 AND "wj_track"."finish_dt" IS NOT NULL
 AND "wj_ecu"."code" = 'VHLSPD'
 AND "wj_ecu"."value" > '0'
ORDER BY "wj_ecu"."date_time" ASC
LIMIT 50




SELECT
   "wj_track"."user_uid" as userID,
   "wj_track"."uuid" as trackID,
   "wj_track"."start_dt" as start_time,
   "wj_track"."finish_dt" as end_time,
   ("wj_track"."finish_dt" - "wj_track"."start_dt") as total,
   "wj_ecu"."value" as speed,
   "wj_ecu"."date_time" as time,
   to_char("wj_ecu"."date_time", 'YYYY-MM-DD HH:MI:SS.MS') as timestamp,
   date_trunc('year', timestamp) as hour,
   date_trunc('month', timestamp) as hour,
   date_trunc('day', timestamp) as hour,
   date_trunc('day', timestamp) as hour,


FROM "wj_track"
JOIN "wj_ecu"
ON "wj_ecu"."track_uid" = "wj_track"."uuid"
WHERE
 "wj_track"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
 AND "wj_track"."start_dt" IS NOT NULL
 AND "wj_track"."finish_dt" IS NOT NULL
 AND "wj_ecu"."code" = 'VHLSPD'
 AND "wj_ecu"."value" > '0'
ORDER BY "wj_ecu"."date_time" ASC
LIMIT 50








SELECT *
FROM "wj_track"
JOIN "wj_ecu" ON "wj_ecu"."track_uid" = "wj_track"."uuid"
WHERE "wj_track"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
 AND "wj_track"."start_dt" IS NOT NULL
 AND "wj_track"."finish_dt" IS NOT NULL
 AND "wj_ecu"."code" = 'VHLSPD'
 AND "wj_ecu"."value" > '0'

ORDER BY "created_by" DESC
LIMIT 50

SELECT * FROM "wj_ecu" WHERE "code" = 'VHLSPD' AND "value" > '0' LIMIT 200


SELECT file.uuid, file.url, video.name, video.user_uid
FROM wj_file as file
JOIN wj_video_file as video_file ON file.uuid = video_file.file_uid
JOIN wj_video as video ON video.uuid = video_file.video_uid
WHERE video.uuid = '671eb3a0-30b3-44b1-8982-cc34f95a79b8';





SELECT car_uid, to_char(updated_at, "dd.mm.yyyy")
FROM "wj_video_fragment" "wj_video_fragment"
WHERE ((wj_video_fragment."deleted_at" is NULL) 
        AND ("wj_video_fragment"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'))
        AND ("wj_video_fragment"."updated_at" >= to_timestamp('1392527428')
        and "wj_video_fragment"."updated_at" <= to_timestamp('1392786628'))
GROUP BY car_uid, to_char(updated_at, "dd.mm.yyyy")
ORDER BY "wj_video_fragment"."updated_at" DESC

/*
SELECT "wj_video_fragment"."car_uid" AS "t0_c6",
       "wj_video_fragment"."uuid" AS "t0_c0",
       "car"."vendor" AS "t1_c3",
       "car"."uuid" AS "t1_c0"
FROM "wj_video_fragment" "wj_video_fragment"
/*LEFT OUTER JOIN "wj_car" "car" ON ("wj_video_fragment"."car_uid"="car"."uuid") AND ("car"."deleted_at" is NULL)*/
/*
WHERE (((wj_video_fragment."deleted_at" is NULL) 
        AND ("wj_video_fragment"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'))
        AND ("wj_video_fragment"."updated_at" >= to_timestamp('1392525437') and "wj_video_fragment"."updated_at" <= to_timestamp('1392784637')))
GROUP BY car_uid, vendor
ORDER BY "wj_video_fragment"."updated_at" DESC
*/

/*
select car_uid, to_char(start_dt, 'dd.mm.yyyy') dt, to_char(start_dt, 'yyyy-mm-dd') dt_for_sort
from wj_video_fragment 
where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') 
and user_uid ='1b2af82d-2d9e-5d03-9da4-bec03904e704'
group by to_char(start_dt, 'dd.mm.yyyy'), car_uid, to_char(start_dt, 'yyyy-mm-dd') 


select t.car_uid,  t.dt, c.vendor, c.model
from (
select car_uid, to_char(start_dt, 'dd.mm.yyyy') dt, to_char(start_dt, 'yyyy-mm-dd') dt_for_sort
from wj_video_fragment 
where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') 
and user_uid ='1b2af82d-2d9e-5d03-9da4-bec03904e704'
group by to_char(start_dt, 'dd.mm.yyyy'), car_uid, to_char(start_dt, 'yyyy-mm-dd') 
) t, wj_car c
where c.uuid = t.car_uid
order by t.dt_for_sort
*/


/*

select car_uid, to_char(start_dt, 'dd.mm.yyyy')
from wj_video_fragment
where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') 
group by to_char(start_dt, 'dd.mm.yyyy'), car_uid
order by to_char(start_dt, 'dd.mm.yyyy');
*/

SELECT file.uuid, file.url, video.name, video.user_uid
FROM wj_file as file
JOIN wj_video_file as video_file ON file.uuid = video_file.file_uid
JOIN wj_video as video ON video.uuid = video_file.video_uid
WHERE video.uuid = '671eb3a0-30b3-44b1-8982-cc34f95a79b8';

UPDATE wj_file SET url = 'https://stat201.wayjournal.com/files/mp4/1/4/1/141758931092907240691.mp4' WHERE wj_file.uuid = '3451360d-1453-44ef-9f62-19a8492ef511';

SELECT file.uuid, file.url, video.name, video.user_uid
FROM wj_file as file
JOIN wj_video_file as video_file ON file.uuid = video_file.file_uid
JOIN wj_video as video ON video.uuid = video_file.video_uid
WHERE video.uuid = '7b5e6557-b96a-4bc4-9bd8-8d9addbee16d';

UPDATE wj_file SET url = 'https://stat201.wayjournal.com/files/mp4/1/4/1/141758888336507648681.mp4' WHERE wj_file.uuid = '683ef9bd-327a-4bb5-9293-9d102a090cd8';




/*SELECT * FROM wj_video_fragment ORDER BY updated_at DESC;*/

/*SELECT video_fragment.uuid
FROM wj_video_fragment as video_fragment
INNER JOIN wj_track as track ON video_fragment.track_uid = track.uuid
INNER JOIN wj_device as device ON track.device_uid = device.uuid
WHERE video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704';
*/

/*
Для поиска видео фрагментов (wj_video_fragment) по устройству (wj_device)

Делаю запрос:
wj_video_fragment => wj_track => wj_device

1) wj_video_fragment => wj_track - находит треки
2) wj_video_fragment => wj_track => wj_device - НЕ находит device 

Вопрос:
Связь ( wj_video_fragment => wj_track => wj_device ) корректно заполняются (существуют)
Если Да , то для кокого пользователя?
*/