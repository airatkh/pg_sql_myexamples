Реализованные виды запросов:

Параметры: 
     * :car_uid='dc86990b-4503-42f1-a907-44ce9b468734' (wj_budget.car_uid)
     * :user_uid='1b2af82d-2d9e-5d03-9da4-bec03904e704'(wj_budget.user_uid)
     * @param string      $period три формата периода: (wj_budget_date_time)
     *                            - Format: dd.mm.yyy   - период определенному дню
     *                            - Format: mm.yyy      - период определенному месяцу в году
     *                            - Format: yyyy        - период за год

Использую sum(wj_budget.total) для подсчета итоговой суммы БЕЗ учета значения валюты.
Четыри варианта запроса.

1) Пример запроса по машине и пользователю, за период годы 
SELECT to_char(wj_budget.date_time, 'yyyy') AS period,
       to_char ( date_trunc('year', wj_budget.date_time ), 'YYYY' )  AS year,
       wj_budget.car_uid,
       sum(total)
FROM "wj_budget"
WHERE wj_budget.car_uid=:car_uid AND wj_budget.user_uid=:user_uid AND wj_budget.deleted_at IS NULL
GROUP BY "period", "year", "car_uid"
ORDER BY "period"

2) Пример запроса по машине и пользователю за месяцы
SELECT to_char(wj_budget.date_time, 'mm.yyyy') AS period,
       to_char(wj_budget.date_time, 'yyyy') AS year,
       to_char( date_trunc('month', wj_budget.date_time ) , 'Month') AS month,
       car_uid,
       sum(total)
FROM "wj_budget"
WHERE wj_budget.car_uid=:car_uid AND wj_budget.user_uid=:user_uid AND wj_budget.deleted_at IS NULL
GROUP BY "month", "period", "year", "car_uid"
ORDER BY "period"

3) Пример запроса по машине и пользователю по дням 
SELECT to_char(wj_budget.date_time, 'dd.mm.yyyy') AS period,
       to_char(wj_budget.date_time, 'yyyy') AS year,
       to_char( date_trunc('day', wj_budget.date_time ) , 'dd') AS day,
       wj_budget.car_uid,
       sum(total)
FROM "wj_budget"
WHERE wj_budget.car_uid=:car_uid AND wj_budget.user_uid=:user_uid AND wj_budget.deleted_at IS NULL
GROUP BY "period", "day", "car_uid", "year"
ORDER BY "period"

4) Вывод информации по дням (возможно отсутствие парапметра car_uid)
Параметры: :user_uid='1b2af82d-2d9e-5d03-9da4-bec03904e704', :car_uid='b3e92f29-382e-4853-889c-a15a486f0876', :period='18.12.2014'
SELECT * FROM "wj_budget" "wj_budget"
WHERE (((wj_budget."deleted_at" is NULL) AND ("wj_budget"."user_uid" = :user_uid)) AND ("wj_budget"."car_uid" = :car_uid)) 
        AND ("wj_budget"."date_time" BETWEEN to_date(:period, 'dd.mm.yyyy') AND to_date(:period, 'dd.mm.yyyy') + (interval '1' day - interval '1' MINUTE)) 
ORDER BY date_time DESC


Поддержка курсов валют
Вариант 1:
currency_uid = false (не указан)
при выполнении запроса надо группировать значение sum с учетом значения currency_uid
Запросс словами: Найти sum всех полей wj_budget.total где wj_budget.currency_uid одинаковый (с возможностью учетом car_uid и user_uid)
Например:
currency_uid = rub
        wj_budget.total = 1
        wj_budget.total = 4
        
currency_uid = dol
        wj_budget.total = 10
        wj_budget.total = 40
currency_uid = eur
        wj_budget.total = 100
        wj_budget.total = 400
        
Результат (массив):
currency_uid = rub
        sum(total) == 5
currency_uid = dol
        sum(total) == 50
currency_uid = eur
        sum(total) == 500
        
Вариант 2:
currency_uid  указан - Надо произвести конвертацию в данную валюту, по курсу(коэффициенту) создания 

Примерный альгоритм:
1) Делаем выборку с учетом user_uid и возможно car_uid за заданный периоуд.
2) Анализируем каждую полученную запись wj_budget
2.1 Проверяем поле wj_budget.currency_uid == заданному параметру сurrency_uid
Если равно: 
sum + = wj_budget.total
Если не равно:
        1) надо сделать конвертацию (например RUB в EUR)
        2) найти ближайший коэффициент где wj_budjet.currency_uid == wj_currency_factor.currency_uid  AND wj_budjet.date_time == (ближайшее время) wj_currency_factor.date_time 
        3) перевести в USD wj_budjet.total / (разделить) wj_currency_factor.value  == USD(wj_budjet.total)
        4) далее перевести в EUR
        5) найти ближайший коэффициент где wj_currency.shot_name == 'EUR' == wj_currency.uuid wj_currency_factor.currency_uid AND wj_budjet.date_time == (ближайшее время) wj_currency_factor.date_time 
        6) перевести в EUR:  полученный в пункте 3 результат  USD(wj_budjet.total) * (умножить) wj_currency_factor.value  = EUR(wj_budget.total)
sum + = EUR(wj_budget.total)

        
Не понятно как результат суммирования сгрупировать по параметру period


