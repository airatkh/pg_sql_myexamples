
select wj_address_house.original_guid
from wj_address_house
inner join
    (select original_guid
     from wj_address_house
     group by original_guid
     having count(*) >= 2) dups 
  on dups.original_guid = wj_address_house.original_guid
  




select count(original_guid)
from wj_address_object_tmp
select original_guid, address_object_uid
from wj_address_house
group by original_guid, address_object_uid
having count(*) >= 2




select original_guid, address_object_uid
from wj_address_house
group by original_guid, address_object_uid
having count(*) >= 2




SELECT *
FROM wj_address_house
WHERE original_guid = '07e31c48-9fe2-4284-bb95-415e19e2d027';


SELECT *
FROM wj_address_house_tmp
WHERE original_guid = '1d4831f6-597c-47ef-8a99-01604b28e29f';



SELECT *
FROM wj_address_house
WHERE original_guid = '0123c5ec-3f20-46e8-9e55-2d66acf6976b';



SELECT count(uuid)
FROM wj_address_house_tmp;


SELECT *
FROM wj_track
WHERE updated_at IS NOT NULL and video_uid IS NOT NULL
ORDER BY updated_at DESC;



SELECT
wj_video_fragment.uuid,
wj_video_fragment.duration
FROM
wj_video_fragment,
wj_video_fragment_file
WHERE
wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_fragment.uuid = '95a96587-952e-5130-adce-cb2a7df5f15f'
AND wj_video_fragment.uuid = wj_video_fragment_file.video_fragment_uid




SELECT date_trunc('year', now());

SELECT double precision date_part('year', date_trunc('year', now()) ) + interval '1 year';

SELECT date_trunc('month', now()); 


SELECT date_part('year', date_trunc('year', wj_video_fragment.start_dt ) ) AS YEAR
FROM wj_video_fragment
GROUP BY YEAR;

SELECT
date_part('year', date_trunc('year', wj_video_fragment.start_dt ) ) AS YEAR,
date_part('month', date_trunc('month', wj_video_fragment.start_dt ) ) AS MONTH
FROM
wj_video_fragment
WHERE
        wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
        AND wj_video_fragment.start_dt > to_date('2013-01-01', 'yyyy-mm-dd') and start_dt < now()
GROUP BY YEAR, MONTH
ORDER BY YEAR, MONTH




select 
  (select min(measured_at)::date from measurements) + ( n    || ' minutes')::interval start_time,
  (select min(measured_at)::date from measurements) + ((n+5) || ' minutes')::interval end_time
from generate_series(0, (24*60), 5) n;



SELECT
wj_video_fragment.uuid,
wj_video_fragment.start_dt,
now() + interval '-1 year' AS one_year_ago
FROM
wj_video_fragment
WHERE
wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_fragment.start_dt BETWEEN date_trunc('year', now() ) AND now()
ORDER BY wj_video_fragment.start_dt DESC


SELECT
wj_video_fragment.uuid,
wj_video_fragment.start_dt,
date_trunc('year', now() )  AS begin_year,
now() AS now
FROM
wj_video_fragment
WHERE
wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_fragment.start_dt BETWEEN date_trunc('year', now() ) AND now()
ORDER BY wj_video_fragment.start_dt DESC



SELECT DISTINCT
wj_video_fragment.car_uid,
to_char(start_dt, 'dd.mm.yyyy') as start_dt
FROM
wj_video_fragment
WHERE
wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_fragment.start_dt BETWEEN to_date('2013.12', 'yyyy.MM') AND to_date('2013.12', 'yyyy.MM') + interval '1' month
GROUP BY start_dt, car_uid


SELECT *
FROM wj_video_user
INNER
WHERE uuid = 'b81b6c7c-c87a-40d8-b814-6f060b284761';


SELECT
to_char(wj_budget.date_time, 'dd.mm.yyyy') AS date,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
AND wj_budget.date_time BETWEEN to_date('03.2014', 'mm.yyyy') AND to_date('03.2014', 'mm.yyyy') + interval '1' month
GROUP BY date, car_uid
ORDER BY date


UPDATE wj_concat_video_task
SET status=0
WHERE status='2'

6a892c7d-47a8-50d3-9a19-e4f426815007

UPDATE wj_modify_task
SET status=0
WHERE uuid = '959af8a3-b928-5879-9087-87e00edbb886'
*


SELECT *
FROM wj_modify_task
WHERE uuid = '959af8a3-b928-5879-9087-87e00edbb886'


SELECT *
FROM wj_concat_video_task
WHERE modify_task_uid = '959af8a3-b928-5879-9087-87e00edbb886';



SELECT *
FROM wj_modify_file
WHERE video_fragment_uid ='3ffe7252-d205-5d85-994a-2e4a4b1b4f4c';
6d1481c6-9342-4a05-9510-cc9521a5844f 



SELECT *
FROM wj_budget
INNER JOIN wj_car
ON wj_budget.car_uid = wj_car.uuid
WHERE wj_budget.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
GROUP BY wj_budget.car_uid. wj_car.uuid

SELECT
to_char(wj_budget.date_time, 'dd.mm.yyyy') AS period,
to_char( date_trunc('day', wj_budget.date_time ) , 'Day') AS str,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.deleted_at IS NULL
GROUP BY period, str, car_uid
ORDER BY period




SELECT
to_char(wj_budget.date_time, 'mm.yyyy') AS period,
to_char( date_trunc('month', wj_budget.date_time ) , 'Month') AS str,
sum(total)
FROM
wj_budget
GROUP BY period, str
ORDER BY period


INSERT INTO 
"wj_budget" 
("date_time", "budget_category_uid", "car_uid", "currency_uid", "total", "description", "user_uid", "track_uid", "uuid",
 "created_at", "updated_at", "deleted_at", "created_by", "updated_by")
VALUES ('1405972800', '1410bb8b-9a90-4deb-9555-48e7d498f7f1', '0ff69a3f-ab69-4039-b758-a28f58be183c', '6670905c-b509-4089-8ab2-c7b78f9306ae',
 '141414141414141', '', '1b2af82d-2d9e-5d03-9da4-bec03904e704', '5d6990f6-863b-413c-ad0f-ef73a9d44997', 'a1bf8fff-5384-411a-a59c-c167f59685b0', 
 'NOW()', 'NOW()', null, '1b2af82d-2d9e-5d03-9da4-bec03904e704', '1b2af82d-2d9e-5d03-9da4-bec03904e704')
 

SELECT
to_char(wj_budget.date_time, 'mm.yyyy') AS month,
to_char( date_trunc('month', wj_budget.date_time ) , 'Mon') AS MONTH_STR,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
GROUP BY month, car_uid,MONTH_STR 
ORDER BY month


By day
SELECT
to_char(wj_budget.date_time, 'dd.mm.yyyy') AS period,
to_char( date_trunc('day', wj_budget.date_time ) , 'Day') AS str,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
GROUP BY period, str, car_uid
ORDER BY period

By month
SELECT
to_char(wj_budget.date_time, 'mm.yyyy') AS period,
to_char( date_trunc('month', wj_budget.date_time ) , 'Mon') AS str,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
GROUP BY period, str, car_uid
ORDER BY period


By year
SELECT
to_char(wj_budget.date_time, 'yyyy') AS period,
to_char ( date_trunc('year', wj_budget.date_time ), 'YYYY' )  AS str,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
GROUP BY period, str, car_uid
ORDER BY period

SELECT
to_char(wj_budget.date_time, 'dd.mm.yyyy') AS date,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
AND wj_budget.date_time BETWEEN to_date('03.2014', 'mm.yyyy') AND to_date('03.2014', 'mm.yyyy') + interval '1' month
GROUP BY date, car_uid
ORDER BY date


ByMonth
SELECT
to_char(wj_budget.date_time, 'mm.yyyy') AS month,
to_char( date_trunc('month', wj_budget.date_time ) , 'Mon') AS MONTH_STR,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
GROUP BY month, car_uid,MONTH_STR 
ORDER BY month


ByDay
SELECT
to_char(wj_budget.date_time, 'dd.mm.yyyy') AS DATE,
wj_budget.car_uid,
sum(total)
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
GROUP BY DATE, car_uid
ORDER BY DATE



SELECT
date_part('year', date_trunc('year', wj_video_fragment.start_dt ) ) AS YEAR,
date_part('month', date_trunc('month', wj_video_fragment.start_dt ) ) AS MONTH,
to_char( date_trunc('month', wj_video_fragment.start_dt ) , 'mon') AS MONTH_STR
FROM
wj_video_fragment
WHERE
wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_fragment.start_dt BETWEEN to_date('2014', 'yyyy') AND to_date('2014', 'yyyy') + interval '1' year
GROUP BY YEAR, MONTH, MONTH_STR
ORDER BY YEAR, MONTH


SELECT
date_part('year', date_trunc('year', wj_track.start_dt ) ) AS YEAR,
date_part('month', date_trunc('month', wj_track.start_dt ) ) AS MONTH,
to_char( date_trunc('month', wj_track.start_dt ) , 'mon') AS MONTH_STR
FROM
wj_track
WHERE
wj_track.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_track.start_dt BETWEEN to_date('2014', 'yyyy') AND to_date('2014', 'yyyy') + interval '1' year
GROUP BY YEAR, MONTH, MONTH_STR
ORDER BY YEAR, MONTH



SELECT
date_part('year', date_trunc('year', wj_track.start_dt ) ) AS YEAR,
date_part('month', date_trunc('month', wj_track.start_dt ) ) AS MONTH,
date_part('day', date_trunc('day', wj_track.start_dt ) ) AS DAY,
to_char( date_trunc('month', wj_track.start_dt ) , 'mon') AS MONTH_STR
FROM
wj_track
WHERE
wj_track.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_track.start_dt BETWEEN to_date('2014.01', 'yyyy.MM.DD') AND to_date('2014.09', 'yyyy.MM.DD')
GROUP BY YEAR, MONTH, DAY ,MONTH_STR
ORDER BY YEAR, MONTH, DAY


date_part('day', date_trunc('day', wj_budget.date_time ) ) AS DAY,

SELECT
to_char(wj_budget.date_time, 'dd.mm.yyyy') AS date_time,
sum(total) as total
FROM
wj_budget
WHERE
wj_budget.car_uid = '0ff69a3f-ab69-4039-b758-a28f58be183c'
GROUP BY date_time
ORDER BY date_time






SELECT
date_part('year', date_trunc('year', wj_video_fragment.start_dt ) ) AS YEAR,
date_part('month', date_trunc('month', wj_video_fragment.start_dt ) ) AS MONTH,
to_char( date_trunc('month', wj_video_fragment.start_dt ) , 'mon') AS MONTH_STR
FROM
wj_video_fragment
WHERE
wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_fragment.start_dt BETWEEN to_date('2014.1.10', 'yyyy.MM.DD') AND to_date('2014.05.20', 'yyyy.MM.DD') + interval '1' year
GROUP BY YEAR, MONTH, MONTH_STR
ORDER BY YEAR, MONTH



AND wj_video_fragment.car_uid = 'fb02e592-fd35-40b0-9dfc-bff9f2144606'


SELECT DISTINCT
wj_video_fragment.car_uid,
to_char(start_dt, 'dd.mm.yyyy') as start_dt
FROM
wj_video_fragment
WHERE
wj_video_fragment.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_fragment.start_dt BETWEEN to_date('2014.05', 'yyyy.MM') AND to_date('2014.05', 'yyyy.MM') + interval '1' month
AND wj_video_fragment.car_uid = 'fb02e592-fd35-40b0-9dfc-bff9f2144606'
GROUP BY start_dt, car_uid








SELECT *
FROM wj_video_fragment
WHERE track_uid = '368b284a-eecf-4534-ad12-a94fbd48cd33';


WHERE uuid = '0ec50301-947f-49bf-8f9c-7828bb381717';

SELECT video_fragment.photo_uid as video_photo_uid, photo.uuid as photo_uid, photo_file.file_uid as phot_file_file_uid, file.*

FROM wj_video_user video_fragment

INNER JOIN wj_photo photo
ON video_fragment.photo_uid = photo.uuid

INNER JOIN wj_photo_file photo_file
ON photo.uuid = photo_file.photo_uid

INNER JOIN wj_file file
ON file.uuid = photo_file.file_uid

WHERE video_fragment.uuid = 'f40a4ac4-44c1-45e4-a5c3-1c75ae2f89fe';



6bef7f07-673c-43fe-824b-1ea4f91adaef



SELECT * 
FROM wj_address_object_tmp
Where fias_aoid = 'ac0a4808-bc1c-4623-bee1-1f1c2c3ce8ac';


select uuid
from wj_address_object_tmp
group by uuid
having count(*) >= 2






SELECT
wj_video_user.uuid,
wj_video_user.duration,
wj_video_user.description
FROM wj_video_user,
wj_video_user_file
WHERE
 wj_video_user.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_user.uuid = '46497ad3-39be-45c6-a995-0ab96b48584d'
AND wj_video_user.uuid = wj_video_user_file.video_user_uid





SELECT
wj_video_user.uuid,
wj_video_user.duration,
wj_video_user.description,
wj_video_user_file.codec,
wj_video_user_file.resolution,
wj_video_user_file.duration,
wj_file.filename,
wj_file.url,
wj_file.path,
wj_file.file_size

FROM wj_video_user,
wj_video_user_file,
wj_file

WHERE
 wj_video_user.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
AND wj_video_user.uuid = 'b81b6c7c-c87a-40d8-b814-6f060b284761'
AND wj_video_user.uuid = wj_video_user_file.video_user_uid
AND wj_video_user_file.file_uid = wj_file.uuid




SELECT *
FROM wj_video_user_file as video_user_file
WHERE video_user_file.video_user_uid = 'bce5cf1a-67dd-4518-8366-0205a66d81c9'




SELECT *
FROM wj_album
WHERE is_fixed = '1' AND is_public = '0';


1)         SELECT
         wj_video_user.uuid,
         wj_video_user.duration,
         wj_video_user.description,
         wj_video_user_file.codec,
         wj_video_user_file.resolution,
         wj_video_user_file.duration
        FROM wj_video_user,
             wj_video_user_file
        WHERE
             wj_video_user.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
        AND wj_video_user.uuid = '7a4ca145-02aa-599c-9eac-db0f388d397c'
        AND wj_video_user.uuid = wj_video_user_file.video_user_uid
        

2)         SELECT
         wj_video_user.uuid,
         wj_video_user.duration,
         wj_video_user.description,
         wj_video_user_file.codec,
         wj_video_user_file.resolution,
         wj_video_user_file.duration,
         wj_file.filename,
         wj_file.url,
         wj_file.path,
         wj_file.file_size
        FROM wj_video_user,
             wj_video_user_file,
             wj_file
        WHERE
             wj_video_user.user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704'
        AND wj_video_user.uuid = '7a4ca145-02aa-599c-9eac-db0f388d397c'
        AND wj_video_user.uuid = wj_video_user_file.video_user_uid
        AND wj_video_user_file.file_uid = wj_file.uuid
        

SELECT *
FROM wj_file
WHERE updated_at IS NOT NULL AND uuid = '85b6f105-9912-4de9-9e0a-6cf08c0e1c6d'
ORDER BY updated_at DESC



SELECT * 
FROM "wj_track" "t" 
WHERE 
("t".start_dt IS NOT NULL AND "t".finish_dt IS NOT NULL) AND 
("t"."user_uid"='e9c7f120-add7-4f51-a1fc-85ac43d000f9') AND
("t"."start_dt" between to_timestamp('0393617600') and to_timestamp('1394740800') )
ORDER BY start_dt
DESC LIMIT 5


SELECT "wj_video_fragment"."uuid" AS "t0_c0",
 "car"."uuid" AS "t1_c0",
  "car"."user_uid" AS "t1_c1",
   "car"."is_default" AS "t1_c2",
    "car"."vendor" AS "t1_c3",
     "car"."model" AS "t1_c4",
      "car"."year" AS "t1_c5",
       "car"."displacement" AS "t1_c6",
        "car"."power" AS "t1_c7",
         "car"."mileage" AS "t1_c8",
          "car"."car_number" AS "t1_c9",
           "car"."photo_uid" AS "t1_c10",
            "car"."album_uid" AS "t1_c11",
             "car"."auto_vendor_uid" AS "t1_c12",
              "car"."auto_model_uid" AS "t1_c13",
               "car"."auto_engine_uid" AS "t1_c14",
                "car"."auto_region_code_uid" AS "t1_c15",
                 "car"."country_uid" AS "t1_c16",
                  "car"."region_uid" AS "t1_c17",
                   "car"."fuel_type" AS "t1_c18",
                    "car"."gasoline" AS "t1_c19",
                     "car"."fuel_tank" AS "t1_c20",
                      "car"."created_at" AS "t1_c21",
                       "car"."updated_at" AS "t1_c22",
                        "car"."deleted_at" AS "t1_c23",
                         "car"."created_by" AS "t1_c24",
                          "car"."updated_by" AS "t1_c25"
FROM "wj_video_fragment" "wj_video_fragment"
  LEFT OUTER JOIN "wj_car" "car" ON ("wj_video_fragment"."car_uid"="car"."uuid")
   AND ("car"."deleted_at" is NULL) 
WHERE ((wj_video_fragment."deleted_at" is NULL)
 AND ("wj_video_fragment"."user_uid" = 'e9c7f120-add7-4f51-a1fc-85ac43d000f9') 
 AND ("car"."user_uid" = 'e9c7f120-add7-4f51-a1fc-85ac43d000f9'))
 ORDER BY "wj_video_fragment"."updated_at" DESC, "car".created_at DESC


SELECT "wj_video_fragment"."uuid" AS
 "t0_c0", "car"."uuid" AS
  "t1_c0", "car"."user_uid" AS
   "t1_c1", "car"."is_default" AS
    "t1_c2", "car"."vendor" AS
     "t1_c3", "car"."model" AS
      "t1_c4", "car"."year" AS
       "t1_c5", "car"."displacement" AS
        "t1_c6", "car"."power" AS
         "t1_c7", "car"."mileage" AS
          "t1_c8", "car"."car_number" AS
           "t1_c9", "car"."photo_uid" AS
            "t1_c10", "car"."album_uid" AS
             "t1_c11", "car"."auto_vendor_uid" AS
              "t1_c12", "car"."auto_model_uid" AS
               "t1_c13", "car"."auto_engine_uid" AS
                "t1_c14", "car"."auto_region_code_uid" AS
                 "t1_c15", "car"."country_uid" AS
                  "t1_c16", "car"."region_uid" AS
                   "t1_c17", "car"."fuel_type" AS
                    "t1_c18", "car"."gasoline" AS
                     "t1_c19", "car"."fuel_tank" AS
                      "t1_c20", "car"."created_at" AS
                      "t1_c21", "car"."updated_at" AS
                       "t1_c22", "car"."deleted_at" AS
                       "t1_c23", "car"."created_by" AS
                        "t1_c24", "car"."updated_by" AS
                        "t1_c25"
FROM "wj_video_fragment" "wj_video_fragment"
  LEFT OUTER JOIN "wj_car" "car"
   ON ("wj_video_fragment"."car_uid"="car"."uuid")
    AND ("car"."deleted_at" is NULL)
WHERE ((wj_video_fragment."deleted_at" is NULL) 
ORDER BY "wj_video_fragment"."updated_at" DESC, "car".created_at DESC



SELECT *
FROM "wj_car" "t"
WHERE ("t"."deleted_at" is NULL)
 AND ("t"."user_uid"='e9c7f120-add7-4f51-a1fc-85ac43d000f9')
ORDER BY "t".created_at DESC




SELECT DISTINCT car_uid, to_char(start_dt, 'dd.mm.yyyy') as start_dt
FROM "wj_video_fragment" "wj_video_fragment"
WHERE ((wj_video_fragment."deleted_at" is NULL)
   AND ("wj_video_fragment"."user_uid" = 'e9c7f120-add7-4f51-a1fc-85ac43d000f9'))
   AND ("wj_video_fragment"."start_dt" >= to_timestamp('1377932800')
   AND "wj_video_fragment"."start_dt" <= to_timestamp('1395478400'))
   AND ("wj_car"."uuid" = '62cf29ec-4fb9-41ef-868a-ba195e0bb9c9')
GROUP BY start_dt, car_uid ORDER BY start_dt DESC





SELECT uuid FROM "wj_video_fragment" "wj_video_fragment"
WHERE (wj_video_fragment."deleted_at" is NULL) AND
      ("wj_video_fragment"."user_uid" = 'e9c7f120-add7-4f51-a1fc-85ac43d000f9')
ORDER BY "wj_video_fragment"."updated_at" DESC


SELECT uuid, start_dt, finish_dt FROM wj_track
WHERE (user_uid = '1b2af82d-2d9e-5d03-9da4-bec03904e704') AND
                    (uuid is not NULL) AND
                    (device_uid is not NULL) AND
                    (start_dt is not NULL) AND
                    (finish_dt is not NULL) AND
                    (created_at is not NULL) AND
                    (updated_at is not NULL) AND
                    (created_by is not NULL) AND
                    (updated_by is not NULL) AND
                    (deleted_at is NULL) AND
                    start_dt between to_timestamp('1393617600') and to_timestamp('1394740800') 
ORDER BY start_dt DESC;


select t.car_uid,  t.dt, c.vendor, c.model
from ( select car_uid, to_char(start_dt, 'dd.mm.yyyy') dt, to_char(start_dt, 'yyyy-mm-dd') dt_for_sort
       from wj_video_fragment 
       where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') 
        and car_uid = 'd39551cc-7ec0-4c3f-93a6-532b83963af0' 
        group by to_char(start_dt, 'dd.mm.yyyy'), car_uid, to_char(start_dt, 'yyyy-mm-dd')) t, wj_car c
where c.uuid = t.car_uid
order by t.dt_for_sort;



select car_uid, to_char(start_dt, 'dd.mm.yyyy')
from wj_video_fragment
where start_dt between to_date('2014-02', 'yyyy-mm') and to_date('2014-03', 'yyyy-mm') and
      car_uid = 'd39551cc-7ec0-4c3f-93a6-532b83963af0'
group by to_char(start_dt, 'dd.mm.yyyy'), car_uid
order by to_char(start_dt, 'dd.mm.yyyy');




SELECT *
FROM wj_device
WHERE uuid = '09bd4419-ada3-4c2c-ab1a-54f0ff2d8dcc';



SELECT uuid, start_dt, finish_dt, device_uid FROM wj_track
                    WHERE (user_uid = '86f33d5f-a6bd-4a7a-8851-7e2c6b79beff') 
                    ORDER BY start_dt DESC



SELECT *
FROM wj_album_entity
WHERE album_uid='3293bcba-b0f7-44bf-be0a-7271796731c7' and album_entity_type_uid='e847da5e-6419-5aa8-83ca-667c3f63cc89' and  entity_uid= '97f5cf02-f337-446e-a453-b2a4e9a7ff78'



INSERT INTO "wj_album_entity" ("seq", "album_uid", "album_entity_type_uid", "entity_uid", "created_at", "updated_at", "deleted_at", "created_by", "updated_by") 
VALUES ('0' ,'3293bcba-b0f7-44bf-be0a-7271796731c7', 'e847da5e-6419-5aa8-83ca-667c3f63cc89', '97f5cf02-f337-446e-a453-b2a4e9a7ff78', 'NOW()', 'NOW()', null, '1b2af82d-2d9e-5d03-9da4-bec03904e704' , '1b2af82d-2d9e-5d03-9da4-bec03904e704')



SELECT *
FROM wj_video_user
ORDER BY updated_at DESC;




INSERT INTO "wj_file" ("uuid", "user_uid", "name", "filename", "path", "file_size", "file_type_uid", "url", "created_at", "updated_at", "deleted_at", "created_by", "updated_by")
VALUES ('f86e76c6-9ee7-4e16-97f1-210d9c355230', 'e9c7f120-add7-4f51-a1fc-85ac43d000f9', '13932323628177882121.mp4.mp4', '13932323768509636958.mp4', '/home/sftpuser/files/mp4/1/3/9', '296728', 'c226f10f-b8f4-54cf-9238-193243904def', 
'http://stat3.wayjournal.com/files/mp4/1/3/9/13932323768509636958.mp4', NOW(), NOW(), null, 'e9c7f120-add7-4f51-a1fc-85ac43d000f9', 'e9c7f120-add7-4f51-a1fc-85ac43d000f9')

ERROR: 13:28:23  [INSERT - 0 row(s), 0.000 secs] [Error Code: 0, SQL State: 23505]  ERROR: duplicate key value violates unique constraint "wj_file_20140221_pkey"
  Detail: Key (uuid)=(f86e76c6-9ee7-4e16-97f1-210d9c355230) already exists.
  Where: SQL statement "INSERT INTO wj_file_20140221 VALUES (NEW.*)"
PL/pgSQL function wj_file_insert_trigger() line 32 at SQL statement
SUMMARY: ... 1 statement(s) executed, 0 row(s) affected, exec/fetch time: 0.000/0.000 sec
 [0 successful, 0 warnings, 1 errors]




:yp0 = f86e76c6-9ee7-4e16-97f1-210d9c355230
:yp1 = e9c7f120-add7-4f51-a1fc-85ac43d000f9
:yp2 = 13932323628177882121.mp4.mp4
:yp3 = 13932323768509636958.mp4
:yp4 = /home/sftpuser/files/mp4/1/3/9
:yp5 = 296728
:yp6 = c226f10f-b8f4-54cf-9238-193243904def
:yp7 = http://stat3.wayjournal.com/files/mp4/1/3/9/13932323768509636958.mp4
:yp8 = NOW()
:yp9 = NOW()
:yp10 = null
:yp11 = e9c7f120-add7-4f51-a1fc-85ac43d000f9
:yp12 = e9c7f120-add7-4f51-a1fc-85ac43d000f9


SELECT "wj_video_fragment"."uuid" AS "t0_c0",
 "wj_video_fragment"."user_uid" AS "t0_c1",
  "wj_video_fragment"."start_dt" AS "t0_c2",
   "wj_video_fragment"."duration" AS "t0_c3",
    "wj_video_fragment"."photo_uid" AS "t0_c4",
     "wj_video_fragment"."track_uid" AS "t0_c5", 
     "wj_video_fragment"."car_uid" AS "t0_c6", 
     "wj_video_fragment"."created_at" AS "t0_c7", 
     "wj_video_fragment"."updated_at" AS "t0_c8", 
     "wj_video_fragment"."deleted_at" AS "t0_c9",
      "wj_video_fragment"."created_by" AS "t0_c10",
       "wj_video_fragment"."updated_by" AS "t0_c11",
        "car"."uuid" AS "t1_c0", 
        "car"."user_uid" AS "t1_c1",
         "car"."is_default" AS "t1_c2",
          "car"."vendor" AS "t1_c3",
           "car"."model" AS "t1_c4", 
           "car"."year" AS "t1_c5", 
           "car"."displacement" AS "t1_c6", 
           "car"."power" AS "t1_c7",
            "car"."mileage" AS "t1_c8", 
            "car"."car_number" AS "t1_c9",
             "car"."photo_uid" AS "t1_c10", 
             "car"."album_uid" AS "t1_c11", 
             "car"."auto_vendor_uid" AS "t1_c12", 
             "car"."auto_model_uid" AS "t1_c13",
              "car"."auto_engine_uid" AS "t1_c14",
               "car"."country_uid" AS "t1_c15", 
               "car"."region_uid" AS "t1_c16", 
               "car"."fuel_type" AS "t1_c17", 
               "car"."gasoline" AS "t1_c18", 
               "car"."fuel_tank" AS "t1_c19",
                "car"."created_at" AS "t1_c20",
                 "car"."updated_at" AS "t1_c21", 
                 "car"."deleted_at" AS "t1_c22",
                  "car"."created_by" AS "t1_c23", 
                  "car"."updated_by" AS "t1_c24"
FROM "wj_video_fragment" "wj_video_fragment"
LEFT OUTER JOIN "wj_car" "car" ON ("wj_video_fragment"."car_uid"="car"."uuid") AND ("car"."deleted_at" is NULL) 
WHERE ((((wj_video_fragment."deleted_at" is NULL)
         AND ("wj_video_fragment"."user_uid" = 'e9c7f120-add7-4f51-a1fc-85ac43d000f9'))
         AND ("wj_video_fragment"."start_dt" >= to_timestamp('01.02.2014', 'dd.mm.yyyy')
         and "wj_video_fragment"."start_dt" <= to_timestamp('19.02.2014', 'dd.mm.yyyy'))) 
         AND ("wj_video_fragment"."car_uid" = 'c95837d3-63c3-43cb-a9c5-1e3972a5a49b'));
         



 f6b97e47-8bfd-41dc-8a84-9ace0500826818.02.2014
28ff5fd5-a525-4374-b0c5-c8d8d49eb81619.02.2014
28ff5fd5-a525-4374-b0c5-c8d8d49eb81619.02.2014

28ff5fd5-a525-4374-b0c5-c8d8d49eb81619.02.2014
28ff5fd5-a525-4374-b0c5-c8d8d49eb81619.02.2014



SELECT "wj_video_fragment"."uuid" AS "t0_c0",
         "wj_video_fragment"."user_uid" AS "t0_c1",
          "wj_video_fragment"."start_dt" AS "t0_c2",
           "wj_video_fragment"."duration" AS "t0_c3",
            "wj_video_fragment"."photo_uid" AS "t0_c4",
             "wj_video_fragment"."track_uid" AS "t0_c5",
              "wj_video_fragment"."car_uid" AS "t0_c6",
               "wj_video_fragment"."created_at" AS "t0_c7",
                "wj_video_fragment"."updated_at" AS "t0_c8",
                 "wj_video_fragment"."deleted_at" AS "t0_c9", 
                 "wj_video_fragment"."created_by" AS "t0_c10", 
                 "wj_video_fragment"."updated_by" AS "t0_c11", 
                 "car"."uuid" AS "t1_c0",
                  "car"."user_uid" AS "t1_c1",
                   "car"."is_default" AS "t1_c2", 
                   "car"."vendor" AS "t1_c3",
                    "car"."model" AS "t1_c4",
                     "car"."year" AS "t1_c5",
                      "car"."displacement" AS "t1_c6",
                       "car"."power" AS "t1_c7", 
                       "car"."mileage" AS "t1_c8",
                        "car"."car_number" AS "t1_c9",
                         "car"."photo_uid" AS "t1_c10", 
                         "car"."album_uid" AS "t1_c11", 
                         "car"."auto_vendor_uid" AS "t1_c12",
                          "car"."auto_model_uid" AS "t1_c13", 
                          "car"."auto_engine_uid" AS "t1_c14",
                           "car"."country_uid" AS "t1_c15",
                            "car"."region_uid" AS "t1_c16",
                             "car"."fuel_type" AS "t1_c17",
                              "car"."gasoline" AS "t1_c18", 
                              "car"."fuel_tank" AS "t1_c19",
                               "car"."created_at" AS "t1_c20",
                                "car"."updated_at" AS "t1_c21",
                                 "car"."deleted_at" AS "t1_c22",
                                  "car"."created_by" AS "t1_c23",
                                   "car"."updated_by" AS "t1_c24" 
FROM "wj_video_fragment" "wj_video_fragment"
LEFT OUTER JOIN "wj_car" "car" ON ("wj_video_fragment"."car_uid"="car"."uuid")
 AND ("car"."deleted_at" is NULL)
WHERE ((((wj_video_fragment."deleted_at" is NULL) 
        AND ("wj_video_fragment"."user_uid" = '1b2af82d-2d9e-5d03-9da4-bec03904e704'))
        AND (to_char("wj_video_fragment"."start_dt",'dd.mm.yyyy') = '19.02.2014') 
        AND ("wj_video_fragment"."car_uid" = '28ff5fd5-a525-4374-b0c5-c8d8d49eb816')));
