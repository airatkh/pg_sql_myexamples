SELECT
  "wj_track"."user_uid" as userID,
  "wj_track"."uuid" as trackID,
  (extract(epoch FROM "wj_track"."finish_dt") - extract(epoch FROM "wj_track"."start_dt")) as track_duration,
  "wj_point"."speed" as speed,
  extract(epoch FROM "wj_point"."date_time") as seconds_datetime
FROM "wj_track"
 JOIN "wj_point"
ON "wj_point"."track_uid" = "wj_track"."uuid"
WHERE
  "wj_track"."user_uid" = 'fc940970-4e38-5334-baf0-acc656f553d6' AND
  "wj_track"."start_dt" IS NOT NULL AND
  "wj_track"."finish_dt" IS NOT NULL AND
  "wj_point"."speed" < '0'
ORDER BY "wj_point"."date_time" ASC


SELECT calls.*
FROM "calls"
 JOIN "proects"
 ON "proects"."id" = "calls"."proectid"
 JOIN "access_users"
 ON "access_users"."organizationid" = "proects"."organizationid"
WHERE "access_users"."id" = 58 AND "calls"."id" = 111
